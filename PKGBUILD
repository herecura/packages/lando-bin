# Maintainer: David Parrish <daveparrish@tutanota.com>

pkgname=lando-bin
pkgver=3.20.4
pkgrel=1
pkgdesc="A free, open source, cross-platform, local development environment and DevOps tool built on Docker container technology"
arch=('x86_64')
url="https://docs.lando.dev/"
license=('GPL')
depends=('docker')
source=('lando')
source_x86_64=("$pkgname-$pkgver.deb::https://github.com/lando/lando/releases/download/v$pkgver/lando-x64-v$pkgver.deb")
sha256sums=('5bafb51749bcd232560398a497499f5029228b35b2cce76fb97030aba915515b')
sha256sums_x86_64=('63398da244f7972b7fcaa817ee1a7210f52a1322762a265c70a3d642fcfa03a9')
conflicts=("lando")
provides=("lando")

# strip breaks executable
options=(!strip)

package(){
    # Extract package data
    tar xf data.tar.gz -C "${pkgdir}"

    install -dm755 "${pkgdir}/usr/lib/lando"
    mv "${pkgdir}/usr/share/lando/bin" \
        "${pkgdir}/usr/lib/lando/"

    # make sure the bundled docker-compose is there
    [[ -e "${pkgdir}/usr/lib/lando/bin/docker-compose" ]]

    # remove "useless" files, we don't need a desktop file for a cli app
    rm -rf "${pkgdir}/usr/share/applications"
    rm "${pkgdir}/usr/share/lando/lando.png"

    # install wrapper
    install -Dm755 "${srcdir}/lando" "${pkgdir}/usr/bin/lando"
}
